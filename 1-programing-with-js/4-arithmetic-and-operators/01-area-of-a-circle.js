#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let radius = Number(prompt("How big is r on the circle? "));
console.log("Area is: " +  Math.pow(radius,2)*Math.PI);
