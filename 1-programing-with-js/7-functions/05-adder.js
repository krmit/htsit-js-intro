#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let my_number = Number(prompt("Number to add 5? Or 3?"));

const adder = function(numberToAdd) {
	return function(param) {return param+numberToAdd};
}

const add5 = adder(5);
const add3 = adder(3);

console.log(add5(my_number));
console.log(add3(my_number));
