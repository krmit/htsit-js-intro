#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let my_number = Number(prompt("Number X to 3*X+5 "));

const mult3 = function(numberValue) {
	return numberValue*3;
}

const adder5 = function(functionA, numberValue) {
	return functionA(numberValue)+5;
}

console.log(adder5(mult3, my_number));
