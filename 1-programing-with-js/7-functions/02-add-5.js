#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let my_number = Number(prompt("Number to add 5? "));

const add5 = function(numberValue) {
	return numberValue+5;
}

console.log(add5(my_number));
