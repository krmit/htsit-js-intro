#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let my_number = Number(prompt("Number X to 3*X+5 "));

const add5 = function(numberValue) {
	return numberValue+5;
}

const mult3 = function(numberValue) {
	return numberValue*3;
}

const combi = function(functionA, functionB) {
	return function(value) {return functionA(functionB(value))};
}

const mult3add5 = combi(add5,mult3); 

console.log(mult3add5(my_number));
