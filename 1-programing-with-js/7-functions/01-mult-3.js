#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let my_number = Number(prompt("Number to mult 3? "));

function mult3(numberValue) {
	return numberValue*3;
}

console.log(mult3(my_number));
