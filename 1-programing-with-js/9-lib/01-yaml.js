#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();
const fs = require('fs');
const yaml = require('js-yaml');


 const data = fs.readFileSync('example.yml', 'utf8');
 const doc = yaml.safeLoad(data);
 
 console.log(doc);
 console.log("Namn:".blue.bold+"  "+doc["id"].red);
 console.log("Ålder:".blue.bold+" "+String(doc["age"]-5).red);


