#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let my_number = Number(prompt("Is it divided by 3 or 5? "));

let post_msg = "Is divided by ";

if(my_number%3===0 && my_number%5===0) {
    console.log(post_msg + "both");
}
else if(my_number%3===0) {
	console.log(post_msg + "3");
}
else if(my_number%5===0) {
	console.log(post_msg + "5");
}
else {
	console.log(post_msg + "none.");
}
