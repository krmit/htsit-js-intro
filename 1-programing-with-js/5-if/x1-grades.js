#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();
const print = require('sprintf-js').sprintf;

let part_i_points   = Number(prompt("What is students points on Part I?  "));
let part_ii_points  = Number(prompt("What is students points on Part II? "));
let part_iii_points = Number(prompt("What is students points on Part III?"));

let total_points=part_i_points+part_ii_points+part_iii_points; 
let c_points=part_ii_points+part_iii_points; 
let a_points=part_iii_points;

// We will now count the number of goals we passt.

let e_goals=0;
let c_goals=0;
let a_goals=0; 

if(total_points>=8) {
    e_goals++;
}

if(total_points>=12) {
    c_goals++;
}

if(c_points>=3) {
    c_goals++;
}

if(total_points>=15) {
    a_goals++;
}

if(c_points>=5) {
    a_goals++;
}

if(a_points===2) {
    a_goals++;
}

let grade;
if(a_goals === 3) {
    grade="A";
}
else if(a_goals === 2 && c_goals === 2) {
    grade="B";
}
else if(c_goals === 2) {
    grade="C";
}
else if(e_goals === 1 && c_goals === 1) {
    grade="D";
}
else if(e_goals === 1) {
    grade="E";
}
else {
    grade="F";
}

console.log("Ditt resultat är: ");
console.log("Totala poäng: "+print("%2d",total_points));
console.log("C poäng:       "+c_points);
console.log("A poäng:       "+a_points);
console.log("Du får bedömningen "+grade+" på provet.")

if(total_points===20) {
    console.log("✨✨⭐️✨✨");
}

if(total_points!==20 && grade==="A") {
    switch(Math.floor(Math.random()*4)) {
        case 0:
            console.log("😆");
            break;
        case 1:
            console.log("😎");
            break;
        case 2:
            console.log("😺");
            break;
        case 3:
            console.log("😋");
            break;
    }
}

if(grade==="B" || grade==="C") {
    console.log("😀") 
}

if(grade==="D" || grade==="E") {
    console.log("☺️") 
}

if(grade==="F") {
    console.log("\nOBS!\nDetta innebär att du härmed är F-varnad.") 
    console.log("Se veckoplaneringen för omprov.");    
}
