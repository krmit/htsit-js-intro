#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

let my_number = Number(prompt("Is number odd or even? "));

let post_msg = "It is ";

if(my_number%2 == 0) {
    console.log(post_msg + "even");
}
else {
	console.log(post_msg + "odd");
}
