#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

const message = function(Name) {
	this.name = Name;
	this.time = null;
	this.welcome = ["Hej", "Tjena", "God dag", "Buuu"][Math.floor(Math.random()*4)];
	this.end_of_line = ["!", "?", ":)"][Math.floor(Math.random()*2)];
	this.print= function() {
		let result="";
		
		if(this.time) {
			result+=this.time+": ";
		}
		
		return result+this.welcome+" "+this.name+this.end_of_line;
	}
}

const my_msg = new message("krm");
my_msg.time=Math.floor(Math.random()*24)+":"+Math.floor(Math.random()*60)+":"+Math.floor(Math.random()*60);

if(my_msg.welcome !== "Buuu") {
	console.log(my_msg.print());
}
else {
	console.log("A troll! Just ignore them.");
}
