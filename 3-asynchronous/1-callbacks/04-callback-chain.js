#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();
const fs = require('fs');
const yaml = require('js-yaml');


 const data = fs.readFile('example.yml', 'utf8', printInfo);
 
 function printInfo(err, value) {
	   if (err) {
           return console.log(err);
       }
       
	  console.log(value);
      const doc = yaml.safeLoad(value);
 
      console.log(doc);
      console.log("Namn:".blue.bold+"  "+doc["id"].red);
      console.log("Ålder:".blue.bold+" "+String(doc["age"]-5).red);
}

