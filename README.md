# Komma igång med intro till JS

  1. Installera git, nodejs och npm. _sudo apt-get install git nodejs npm_
	1. Om du är på debian/ubuntu/mint eller likande behöver du använda följande kommando för att kunna köra skripten direkt:
	_sudo ln -s /usr/bin/nodejs /usr/bin/node_
  2. Klona detta projekt: _git clone https://gitlab.com/krmit/htsit-js-intro.git_.
  3. Gå in i arkivet _cd htsit-js-intro_.
  4. Installera nodejs paket med hjälp av npm. _npm install_
  5. Updatera arkivet ofta och passa på att även göra steg fyra då och då. _git pull_
  6. Du kan köra scripten genom att skriva "node script.js" eller "./script.js" beronde på vilken arkitektur du är på.


