#!/usr/bin/node
"use strict"

require("colors");
const prompt=require("prompt-sync")();

printAplusB(1,1,2);
printAplusB(0,0,0);
printAplusB(0.1,0.2,0.3) //false
printAplusB(0.1,0.4,0.5)
printAplusB(0.2,0.4,0.6) //false
printAplusB(0.3,0.3,0.6)

console.log("This small error could have big impact. See below:");
console.log(String(((0.2+0.4)-(0.3+0.3))*100000000000000000).yellow);
console.log(String(((2+4)-(3+3))/10*100000000000000000).yellow);

function printAplusB(A,B, compare) {
   let result_sum=A+B;
   let result = String(A)+"+"+String(B)+"=" +String(result_sum);
   if(result_sum===compare) {
       result=result.green;
   }
   else {
	   result=result.red;
   }
   console.log(result)
}
